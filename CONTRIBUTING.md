# Contributing

## Forking

You may use this repository to store any results from your hackathon project.
Please [fork it on GitLab][1], push your change to a named branch, then send us
a pull request.

In order to keep things organized, save your results (code, documents, data,
etc.) in a new subdirectory with the name of your project or team. Your
subdirectory should at least contain a README.md file with basic information
about the results (What is it? How does it work? How do you use it? Etc.).

[1]: https://gitlab.com/arise-biodiversity/hackathon/arise-hackathon-november-2022/-/forks/new

## Branching

Another way of contributing is by branching of this project.
You can create a local development branch:

```
git checkout -b name-of-your-branch
```

Commit your changes and push it to gitlab:

```
git push origin
```

The push will give you a link to a form to create your merge request.
This you can share with your colleagues or you can use to discuss
your changes.

When you want to merge you can press the 'merge' button or ask one
of the maintainers of the project to do so.


## Rebasing

Sometimes the main branch is already a few steps ahead and your changes
cannot be merged or will lead to merge conflicts.
You can fix this by doing the following.

Fetch the most recent main commits:

```
git fetch origin main:main
```

Rebase on the main branch:

```
git rebase main
```

Force push to overwrite your own branch:

```
git push --force <name of your branch>
```

If you do experience merge conflicts, resolve them during rebase.
Never commit or force push conflicts.

## Branching your branch

You can also branch on your own branch, but be sure to create merge requests
that point to your own branch and not to the main.

## Housekeeping rules

* Use the description to describe what your adding and why and how other can use/test it.
* Use the comments on an MR's to discuss specifics of a merge request.
* Create Draft MR's by adding 'Draft: ' to the title of your branch.
* Remove branches that are no longer relevant.

