## Sequencing inputs and objects

You can find the details in that document:

https://drive.google.com/drive/folders/1wCWsSVDopjpzItTzd1Xv-zSqVInh7FY_?usp=share_link

>note: many models are not relevant for the hackathon and to the end users, e.g. the ones about the pipelines or barcode Index. Use the documents
[below](/README.md#Mock_data).



## Mock data

Barcodes sequences and metadata from 20 sequencing runs were generated and flattened into the following json files:

`specimens.json`: specimens object. The taxon name is not randomly generated so it will be formatted as "species name of RMNH.5000001"  
`lims.json`: information about the processing of the barcodes, linked to specimen data through  the "specimen" key.  
`barcodes.json`: information about the barcodes, linked to lims data through  the "lims" key.  

The files are available here: https://drive.google.com/drive/folders/1kK_zATHBG6_JBbgvyi2359e3EnTsHDFA?usp=share_link

## COI sequences from BOLD

If you need real sequences with correct taxonomy, you can find 6M+ COI barcode sequences from [BOLD](https://v4.boldsystems.org/index.php), put together in a single fasta file there:

https://drive.google.com/file/d/1bCLsPcqsEXUcAxIcXzHFBltqWz1xUPQF/view?usp=share_link

The fasta headers look like this:

```
>processid:GBDP32839-19; sampleid:KT027004; genbank:KT027004; seqid:11742297.0; marker:COI-5P; country:Burkina Faso | k:Animalia; p:Arthropoda; c:Insecta; o:Diptera; f:Ceratopogonidae; sf:Ceratopogoninae; g:Culicoides; sp:Culicoides imicola; ssp:none
```
The first part contains some annotations:
`processid`, `sampleid`, `genbank`, `seqid` are \~unique IDs to trace the data back to BOLD, Genbank or submitters
`marker`: "COI-5P" for all sequences
`country`: sampling location

The second part (after the `|`) contains the taxonomy information:
`k`: kingdom
`p`: phylum
`c`: class
`o`: order
`f`: family
`sf`: subfamily
`g`: genus
`sp`: species
`ssp`: subspecies

>Notes:
> 1. there are about 9400 sequences from Naturalis Specimen (search for "RMNH.")
> 2. the NSR taxonomy (so ARISE data) have only 7 taxon levels: k, p, o, c, f, g, sp
> 3. keep in mind that the taxonomy from BOLD might not match the NSR taxonomy


## NSR taxonomy tree

A table with taxonomy tree of the <a href="https://www.nederlandsesoorten.nl/">Dutch Species Register</a> is available here:

https://drive.google.com/file/d/1tS_C5yx8QNH5jZUwnCvRIjhGRX_7s9Ia/view?usp=share_link

reach me (Pierre) is you need more info about the content of that table.

## Subset of CRS data species names

In a past project, Naturalis BOLD sequences (9400 + other non-COI-5P) were matched against NSR,

The list of species names for these IDs are available here:  https://drive.google.com/file/d/1iPl6WjaWiG8bmhZK75DZ1PiNdMuj_mES/view?usp=share_link


## fasta files and json metadata for IRODS

About 50k fasta sequences stored in separate files were generated from the [20 mock sequencing run](/README.md#Mock_data). you can find them here:

https://drive.google.com/file/d/1LpPLIpJqLjUNIGWkjeV-U9jz---WUWLg/view?usp=share_link

Each .fasta has a .json file with some metadata, to test the IRODS querying system.

## End-user queries

Give me:
 - sequences from Diptera >600bp
 - sequences from specimens from the North Sea
 - Dutch COI sequences from specimens sequenced in 2022
 - sequences from a specific Nanopore run
 - sequences from stock plate X or PCR Y or extract Z
 - sequences from a specific project
 - sequences from specimens collected by X in 2021

Or any combination on these criteria.

Currently not possible:
  - Dutch COI sequences generated in 2022
  - COI sequences from Kaderrichtlijn species
  - validated marine COI sequences
