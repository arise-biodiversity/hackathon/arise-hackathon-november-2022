# Generating enriched data from the raw data

The enriched data design can be found here: <https://dbdiagram.io/d/6374d218c9abfc6111731959>

The Python script generating enriched data from the raw data can be found at [code/biocloud-delta/scripts/raw_to_enriched.py](code/biocloud-delta/scripts/raw_to_enriched.py).

## Description of the files
  
| File         | Description |
|--------------|---------------|
| Raw_to_DeltaTable.ipynb | Jupyter notebook explaining steps to generate enriched data           |
| pyproject.toml &  poetry.lock    | describes dependencies (if you are using poetry)         |
| requirements.txt      | dependencies          |

## Requirements

The dependencies can be installed by running:

```
pip install -r requirements.txt
```

If you are using [poetry](https://python-poetry.org/docs/), you can install the dependencies by running:

```
poetry install
```

You can activate the virtual environment by running:
```
poetry shell
```

## Jupyter notebook

To run the Jupyter notebook run:
```
jupyter-lab
```
