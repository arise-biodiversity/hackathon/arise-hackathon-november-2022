#!/usr/bin/env bash

# Github: https://github.com/wtsi-npg/baton
# Man page: http://wtsi-npg.github.io/baton/

set -eux -o pipefail

installdir="$(mktemp -d)"
cd "${installdir}"

apt-get -qq update
apt-get -y --no-install-recommends install \
    curl \
    build-essential \
    libtool \
    libcurl4-openssl-dev \
    cpanminus \
    ca-certificates \
    libssl-dev \
    irods-dev \
    jq

cd "${installdir}"
curl -L https://github.com/akheron/jansson/releases/download/v2.14/jansson-2.14.tar.gz | tar -xz
cd jansson-2.14
./configure
make
make install

cd "${installdir}"
curl -L https://github.com/wtsi-npg/baton/releases/download/3.2.0/baton-3.2.0.tar.gz | tar -xz
cd baton-3.2.0
CPPFLAGS="-I/usr/include/irods" \
    LDFLAGS="-L/usr/lib/irods" ./configure
make CPPFLAGS="-I/usr/include/irods" \
    LDFLAGS="-L/usr/lib/irods"
make install

/sbin/ldconfig

rm -rf "${installdir}"
