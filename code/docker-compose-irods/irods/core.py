#!/usr/bin/python

import datetime
import json
import subprocess
import exifread
from genquery import *

# Read exif data from images on iput and add as avu's
def pep_api_data_obj_put_post (rule_args,callback,rei):
    objpath = str(rule_args[2].objPath)
    coll_name = '/'.join(objpath.split('/')[:-1])
    data_name = objpath.split("/")[-1]
    for path in Query(callback, columns=('DATA_PATH'), conditions="COLL_NAME = '{0}' and DATA_NAME = '{1}'".format(coll_name,data_name)):
        print(path)

        with open(path, 'rb') as f:
            try:
                tags = exifread.process_file(f)
                kvp = irods_types.KeyValPair()
                for (k, v) in tags.items():
                    if k not in ('JPEGThumbnail','TIFFThumbnail','Filename','EXIF MakerNote'):
                        kvp = callback.msiAddKeyVal(kvp, k, str(v)) ['arguments'] [0]
                callback.msiSetKeyValuePairsToObj(kvp, objpath, "-d")
            except:
                pass

# Read content of file on adding metadata with 'iget yourfile -'
#def pep_database_set_avu_metadata_pre(rule_args, callback, rei):
#    #callback.writeLine("serverLog", "pep_database_set_avu_metadata_pre. Arguments: " + str(len(rule_args)))
#
#    #object_type = rule_args[3]
#    object_path = rule_args[4]
#    #object_attribute = rule_args[5]
#    #object_value = rule_args[6]
#    #object_unit = rule_args[7]
#
#    #ret_val = callback.msiSplitPath(object_path, "", "")
#    #data_object = ret_val['arguments'][2]
#    #collection = ret_val['arguments'][1]
#
#    #print(collection)
#    #print(data_object)
#
#    args = ['iget', object_path, '-']
#
#    try:
#        p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#        (stdout, stderr) = p.communicate()
#        print(stdout)
#    except (subprocess.CalledProcessError, OSError ) as err:
#        exit(1)
#    if p.returncode != 0:
#        print("Failure in returncode of command")
