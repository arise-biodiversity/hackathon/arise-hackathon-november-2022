### iRODS

Create 1000 dummyfiles of 1 byte and upload:
```
docker compose exec irods bash
su irods
mkdir /tmp/test.1
cd /tmp/test.1
seq -w 1 1000 | xargs -n1 -I% sh -c 'dd if=/dev/zero of=file.% bs=1 count=1'
iput -r /tmp/test.1
```

Add metadata to collection:
```
imeta add -C test.1 project_id 1
imeta add -C test.1 deployment_id 1
```

Connect to iRODS from host (Ubuntu 18.04 & 20.04 only):
```
cat ~/.irods/irods_environment.biocloud.json

{
    "irods_host": "localhost",
    "irods_port": 1247,
    "irods_user_name": "rods",
    "irods_zone_name": "tempZone",
    "irods_default_resource": "demoResc"
}

export IRODS_ENVIRONMENT_FILE=~/.irods/irods_environment.biocloud.json
iinit
```
Password:
```
irods
```

Add iRODS resource (storage):
```
docker compose exec irods bash
mkdir -p /mnt/irods/dataResc
chown -R irods:irods /mnt/irods/dataResc
su irods
iadmin mkresc dataResc unixfilesystem irods:/mnt/irods/dataResc
```
