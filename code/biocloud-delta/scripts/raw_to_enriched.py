#!/usr/local/bin/python
import os
import sys
import pyspark
from delta import configure_spark_with_delta_pip
from dotenv import load_dotenv
import json
import glob
import random
from sequencing_raw import config_spark

def import_raw_tables(spark, import_path):
    def file_path(table):
        return os.path.join(import_path, f"{table}_raw")
    barcodes_df = spark.read.format("delta").load(file_path("barcodes"))
    barcodes_df.write.format("delta").mode("overwrite").saveAsTable("raw_barcodes")

    lims_df = spark.read.format("delta").load(file_path("lims"))
    lims_df.write.format("delta").mode("overwrite").saveAsTable("raw_lims")

    locations_df = spark.read.format("delta").load(file_path("locations"))
    locations_df.write.format("delta").mode("overwrite").saveAsTable("raw_locations")

    specimens_df = spark.read.format("delta").load(file_path("specimens"))

    # NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE
    print('WARN: The database location is fake and needs to be fixed')
    # The specimens don't have a location_id in this mock data, so we are creating a random one
    from pyspark.sql.functions import monotonically_increasing_id, row_number
    from pyspark.sql import Window

    specimens_count = specimens_df.count()
    locations_count = locations_df.count()
    def randint_or_none(maxint):
        r = random.randint(0, 2 * maxint) + 1
        if r > maxint:
            r = None
        return r

    location_ids = [
        randint_or_none(locations_count)
        for _ in range(0, specimens_count)
    ]
    tmp_locations_df = spark.createDataFrame([(l,) for l in location_ids], ["location_id"])

    #add 'sequential' index and join both dataframe to get the final result
    specimens_df = specimens_df.withColumn("row_idx", row_number().over(Window.orderBy(monotonically_increasing_id())))
    tmp_locations_df = tmp_locations_df.withColumn("row_idx", row_number().over(Window.orderBy(monotonically_increasing_id())))

    specimens_df = specimens_df.join(tmp_locations_df, specimens_df.row_idx == tmp_locations_df.row_idx).drop("row_idx")
    # END NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE
    specimens_df.write.format("delta").mode("overwrite").saveAsTable("raw_specimens")

def read_nsr_table(spark, nsr_path):
    file_path = os.path.join(nsr_path, "NSR_nodes.tsv")
    nsr_df = spark.read.format("csv").options(delimiter="\t", header=True).csv(file_path)
    nsr_df.write.format("delta").mode("overwrite").saveAsTable("NSR")

def create_enriched_tables(spark, spark_path):
    print('WARN: Locations need to be fixed')
    spark.sql(f"""
        CREATE TABLE IF NOT EXISTS locations (
            id INT NOT NULL,
            lat DOUBLE NOT NULL,
            lon DOUBLE NOT NULL,
            name STRING
        )
        USING DELTA
        LOCATION '{os.path.join(spark_path, "locations_table")}'
        """)

    spark.sql(f"""
        CREATE TABLE IF NOT EXISTS specimens (
            id STRING NOT NULL,
            taxon STRING,
            taxon_id INT NOT NULL,
            location_id INT
        )
        USING DELTA
        LOCATION '{os.path.join(spark_path, "specimens_table")}'
        """)

    spark.sql(f"""
        CREATE TABLE IF NOT EXISTS lims (
            id INT NOT NULL,
            specimen STRING NOT NULL,
            specimen_name STRING NOT NULL,
            sequencing_run STRING,
            marker STRING,
            microplate_id STRING,
            microplate_position STRING,
            extract_id STRING,
            pcr_id STRING,
            forward_primer STRING,
            reverse_primer STRING,
            pcr_index STRING,
            dual_index STRING
        )
        USING DELTA
        LOCATION '{os.path.join(spark_path, "lims_table")}'
        """)

    spark.sql(f"""
        CREATE TABLE IF NOT EXISTS barcodes (
            id INT NOT NULL,
            barcoding_run STRING,
            lims INT NOT NULL,
            specimen_name STRING NOT NULL,
            forward_primer_sequence STRING,
            reverse_primer_sequence STRING,
            sequence STRING,
            length INT
        )
        USING DELTA
        LOCATION '{os.path.join(spark_path, "barcodes_table")}'
        """)

def upsert_locations_table(spark):
    spark.sql("""
        MERGE INTO locations
        USING raw_locations
        ON locations.id = raw_locations.id
        WHEN MATCHED THEN
            UPDATE SET
                id = raw_locations.id,
                lat = raw_locations.latitudeDecimal,
                lon = raw_locations.longitudeDecimal,
                name = raw_locations.name
        WHEN NOT MATCHED THEN
            INSERT (
                id,
                lat,
                lon,
                name
            )
            VALUES (
                raw_locations.id,
                raw_locations.latitudeDecimal,
                raw_locations.longitudeDecimal,
                raw_locations.name
            )
        """)

def upsert_specimens_table(spark):
    print("Warn: taxon_id is being ignored")
    # This is what we had before:
    # USING (SELECT * FROM raw_specimens LEFT JOIN NSR ON raw_specimens.taxon = NSR.name GROUP BY NSR.name)
    # However, there are duplicate entries for a given NSR.name
    spark.sql("""
        MERGE INTO specimens
        USING raw_specimens
        ON specimens.id = raw_specimens.id
        WHEN MATCHED THEN
            UPDATE SET
                taxon = raw_specimens.taxon,
                taxon_id = NULL,
                location_id = raw_specimens.location_id
        WHEN NOT MATCHED THEN
            INSERT (
                id,
                taxon,
                taxon_id,
                location_id
            )
            VALUES (
                raw_specimens.id,
                raw_specimens.taxon,
                NULL,
                raw_specimens.location_id
            )
        """)

def upsert_lims_table(spark):
    spark.sql("""
        MERGE INTO lims
        USING (SELECT * FROM raw_lims LEFT JOIN specimens ON raw_lims.specimen = specimens.id)
        ON lims.id = raw_lims.id
        WHEN MATCHED THEN
            UPDATE SET
                id = raw_lims.id,
                specimen = raw_lims.specimen,
                specimen_name = specimens.taxon,
                sequencing_run = raw_lims.sequencing_run,
                marker = raw_lims.marker,
                microplate_id = raw_lims.microplate_id,
                microplate_position = raw_lims.microplate_position,
                extract_id = raw_lims.extract_id,
                pcr_id = raw_lims.pcr_id,
                forward_primer = raw_lims.forward_primer,
                reverse_primer = raw_lims.reverse_primer,
                pcr_index = raw_lims.pcr_index,
                dual_index = raw_lims.dual_index
        WHEN NOT MATCHED THEN
            INSERT (
                id,
                specimen,
                specimen_name,
                sequencing_run,
                marker,
                microplate_id,
                microplate_position,
                extract_id,
                pcr_id,
                forward_primer,
                reverse_primer,
                pcr_index,
                dual_index
            )
            VALUES (
                raw_lims.id,
                raw_lims.specimen,
                specimens.taxon,
                raw_lims.sequencing_run,
                raw_lims.marker,
                raw_lims.microplate_id,
                raw_lims.microplate_position,
                raw_lims.extract_id,
                raw_lims.pcr_id,
                raw_lims.forward_primer,
                raw_lims.reverse_primer,
                raw_lims.pcr_index,
                raw_lims.dual_index
            )
        """)

def upsert_barcodes_table(spark):
    spark.sql("""
        MERGE INTO barcodes
        USING (SELECT * FROM raw_barcodes LEFT JOIN lims ON raw_barcodes.lims = lims.id)
        ON barcodes.id = raw_barcodes.id
        WHEN MATCHED THEN
            UPDATE SET
                id = raw_barcodes.id,
                barcoding_run = raw_barcodes.barcoding_run,
                lims = raw_barcodes.lims,
                specimen_name = lims.specimen_name,
                forward_primer_sequence = raw_barcodes.forward_primer_sequence,
                reverse_primer_sequence = raw_barcodes.reverse_primer_sequence,
                sequence = raw_barcodes.sequence,
                length = raw_barcodes.length
        WHEN NOT MATCHED THEN
            INSERT (
                id,
                barcoding_run,
                lims,
                specimen_name,
                forward_primer_sequence,
                reverse_primer_sequence,
                sequence,
                length
            )
            VALUES (
                raw_barcodes.id,
                raw_barcodes.barcoding_run,
                raw_barcodes.lims,
                lims.specimen_name,
                raw_barcodes.forward_primer_sequence,
                raw_barcodes.reverse_primer_sequence,
                raw_barcodes.sequence,
                raw_barcodes.length
            )
        """)

def save_enriched_tables(spark, export_path):
    for table in ["locations", "specimens", "lims", "barcodes"]:
        file_path = os.path.join(export_path, f"{table}_enriched")
        spark.table(table).write.format("delta").mode("overwrite").save(file_path)

def main():
    load_dotenv()

    raw_path = "/export"
    nsr_path = "/import"
    spark_path = "/spark"
    enriched_path = "/export"

    spark = config_spark(spark_path)

    # Importing raw and reference data
    import_raw_tables(spark, raw_path)
    read_nsr_table(spark, nsr_path)

    # Creating the spark tables
    create_enriched_tables(spark, spark_path)

    upsert_locations_table(spark)
    upsert_specimens_table(spark)
    upsert_lims_table(spark)
    upsert_barcodes_table(spark)

    save_enriched_tables(spark, enriched_path)

if __name__ == "__main__":
    main()
