#!/usr/local/bin/python
import os
import sys
import logging
import irods.exception as irods_ex
from irods.session import iRODSSession
from typing import Dict, List, Tuple, Any, Optional
import requests
import boto3

class Diopsis:
  """ Receive payload from ai.naturalis.nl server"""

  def __init__(self, url: str, password: str):
      self.url = url
      self.password = password
      self._token = None

  def login(self):
      self._token = requests.post(
          f"{self.url}/authentication",
          data={"username": "concurrentuser", "password": self.password},
          timeout=(5, 30),
      ).json()["secure_token"]

  def image_ids_from_camera(self, camera_number:str) -> List[str]:
      image_ids = requests.get(
          f"{self.url}/projects/insectcam_insectpi{camera_number}"
          f"/groups/__all__/images?secure_token={self._token}"
      ).json()["items"]
      return image_ids

  def image_data(
      self, camera_number: str, image_id: str, retry: bool = True
  ) -> Optional[bytes]:
      media_request = requests.get(
          f"{self.url}/projects/insectcam_insectpi{camera_number}"
          f"/groups/__all__/images/{image_id}/media?secure_token={self._token}"
      )
      if media_request.headers["Content-length"] == "0":
          logging.warning(
              f"SKIPPING: No image content for <{image_id}> "
              f"- status code = {media_request.status_code}"
          )
          return None
      elif media_request.status_code != 200:
          if retry:
              logging.warning(
                  f"status code is not 200 => {media_request.status_code}"
                  " - trying to login again"
              )
              self.login()
              return self.image_data(camera_number, image_id, retry=False)
          else:
              logging.warning(
                  f"status code is not 200 => {media_request.status_code} - no RETRY"
              )
              return None
      else:
          return media_request.content

class Faunabit:
  """ Receive faunabit data and store in local folder """
  def __init__(self, url: str, token: str):
    self.FAUNABIT_API_URL = url
    self.FAUNABIT_API_TOKEN = token
    self.FAUNABIT_API_TYPES = ['locations', 'researches', 'devices']

  def get_faunabit_data(self, type):
    try:
      # Fetch metadata JSON files from Faunabit
      # for type in self.FAUNABIT_API_TYPES:
        header =  {"Authorization": "Bearer " + self.FAUNABIT_API_TOKEN}
        response = requests.get(self.FAUNABIT_API_URL + type, headers=header)

        if response.status_code == 200:
          return response.json()
        else:
          raise Exception('Faunabit ' + type + ' was not found or an error occured.')

    except Exception as e:
      exc_type, exc_obj, exc_tb = sys.exc_info()
      fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
      print('An unexpected error occurred: ' + str(e) + ' - ' + str(exc_type) + ', ' + str(fname) + ', ' + str(exc_tb.tb_lineno))

class S3:
  """ S3 connection class (for sending metadata) """

  def __init__(self, s3_credentials):
    self.key_id = s3_credentials["s3_key_id"] if  s3_credentials["s3_key_id"] else os.getenv("S3_KEY_ID")
    self.secret = s3_credentials["s3_secret"]
    self.bucket = s3_credentials["s3_bucket"]
    self.region = s3_credentials["s3_region"]
    self.client = None

  def connect(self):
    try:
      s3 = boto3.Session(
            aws_access_key_id=self.key_id,
            aws_secret_access_key=self.secret,
            region_name=self.region
        )
      self.client = s3.client('s3')
      return True
    except Exception as e:
      print(str(e))
      return False

  def list_objects(self):
    print("=== LIST S3 OBJECTS ===")
    objects = []
    try:
      resp = self.client.list_objects(Bucket=self.bucket)
      files = resp.get("Contents")
      for file in files:
        print(file['Key'])
        objects.append(file['Key'])
      print(f"Objects in bucket: {str(len(files))} files found\n")
      return objects
    except:
      print("No objects found in this bucket\n")

  def get_object(self, key):
    print(f"=== GET OBJECT: {key} ===")
    try:
      obj = self.client.get_object(Bucket=self.bucket, Key=(key))
      return obj['Body'].read().decode('utf-8')
    except Exception as e:
      print(f"{key} not found in bucket {self.bucket}\n")
      print(str(e))
      return False

  def remove_objects(self, folder, files):
    print("=== REMOVE S3 OBJECTS ===")
    try:
      for file in files:
        key = f"{folder}/{file}"
        self.client.delete_object(Bucket=self.bucket, Key=(key))
        print(f"{folder}/{file} removed from bucket {self.bucket}")
      return True
    except Exception as e:
      print(f"{folder}/{file} not found in bucket {self.bucket}\n")
      print(str(e))
      return False

  def empty_bucket(self):
    print("=== REMOVE S3 OBJECTS ===")
    count = 0
    try:
        resp = self.client.list_objects(Bucket=self.bucket)
        files = resp.get("Contents")
        for file in files:
          print("remove: ",file['Key'])
          self.client.delete_object(Bucket=self.bucket, Key=(file['Key']))
          count +=1
        print("Removed ",count, "objects from ",self.bucket)
        return True
    except Exception as e:
      print(str(e))
      return False

  def store_objects(self, source_folder, folder, files):
    print("=== STORE S3 OBJECTS ===")
    try:
      for file in files:
          key = f"{folder}/{file}"
          filename = f"{source_folder}/{file}"
          self.client.upload_file(Filename=filename, Bucket=self.bucket, Key=(key))
      print("Metadata succesfully stored in landing zone\n")
      return True
    except FileNotFoundError:
      print("The local file was not found")
      return False
    except Exception as e:
      print("Metadata could not be stored in landing zone\n")
      print(str(e))
      return False

class Irods:
  """ Irods connection class (for sending payload) """

  def __init__(self, irods_credentials):
    self.session = None
    cv = irods_credentials
    self.host = cv['host']
    self.port = cv['port']
    self.user = cv['user']
    self.zone = cv['zone']
    self.dflt_resc = cv['default_resource']
    try:
        self.session = iRODSSession(
            host=cv['host'], port=cv['port'], user=cv['user'],
            password=cv['password'], zone=cv['zone'])
    except irods_ex.iRODSException as e:
        print("IRODS connection fails:")
        print(str(e))
        sys.exit(1)

  def info(self):
    try:
        return f"IRODS[h={self.host},p={self.port},u={self.user},z={self.zone}]"
    except Exception:
        return "FAILED-IRODS-CONNECTION"

  def exists_collection(self,collection_path,**options):
    with self.session as session:
      try:
          session.collections.get(collection_path)
          return True
      except irods_ex.CollectionDoesNotExist:
          return False

  def create_collection(self,collection_path,**options):
    with self.session as session:
      logging.info('IRODS: create_collection(collection_path=%s)',collection_path)
      session.collections.create(collection_path)

  def remove_collection(self,collection_path,**options):
    with self.session as session:
      logging.info('IRODS: remove_collection(collection_path=%s)',collection_path)
      session.collections.remove(collection_path)

  def remove_all_collection_objects(self,collection,**options):
    for object in self.ls(collection):
      self.remove_object(os.path.join(collection,object))

  def remove_object(self,object_path,**options):
    with self.session as session:
      # logging.info('IRODS: remove_object(object_path=%s)',object_path)
      session.data_objects.unlink(object_path)

  def ls_objects(self,directory,**options):
    with self.session as session:
      # logging.info('IRODS: ls_objects(directory=%s)',directory)
      coll = session.collections.get(directory)
      return coll.data_objects

  def ls(self,directory,**options):
      print("ls: ",[obj.name for obj in self.ls_objects(directory,**options)])
      print ("metadata: ",[obj.metadata.items() for obj in self.ls_objects(directory,**options)])
      return [obj.name for obj in self.ls_objects(directory,**options)]
