#!/usr/local/bin/python
import os
import pyspark
from delta import *
from irods.session import iRODSSession
from dotenv import load_dotenv


def get_image_metadata():
    HOST = os.getenv("IHOST")
    PORT = os.getenv("IPORT")
    USER = os.getenv("IUSER")
    PASS = os.getenv("IPASS")
    ZONE = os.getenv("IZONE")
    ROOT = os.getenv("ISTORAGE_ROOT")
    with iRODSSession(host=HOST, port=PORT, user=USER, password=PASS, zone=ZONE) as session:
        sensor_collection = session.collections.get(f"{ROOT}/sensors")
        collections = sensor_collection.subcollections
        media_item_metadata = []
        for collection in collections:
            sensor_id = collection.metadata.get_one("sensor_id")
            for media_item in collection.data_objects:
                media_item_metadata.append({
                    "id": sensor_id.value,
                    "id_name": collection.name,
                    "datetime": media_item.metadata.get_one("datetime").value,
                    "mime_type": media_item.metadata.get_one("mime-type").value,
                    "uri": f"{ROOT}/sensors/{collection.name}/{media_item.name}"
                })
        return media_item_metadata


def config_spark():
    SPARK_MASTER = "local[1]"
    AWS_ACCESS_KEY = os.getenv("S3_KEY_ID")
    AWS_SECRET_KEY = os.getenv("S3_SECRET")

    builder = pyspark.sql.SparkSession.builder.appName("Biocloud").master(SPARK_MASTER) \
            .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
            .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
            .config("fs.s3a.impl","org.apache.hadoop.fs.s3a.S3AFileSystem") \
            .config("spark.hadoop.fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider") \
            .config("fs.s3a.endpoint", "s3.amazonaws.com") \
            .config("spark.hadoop.fs.s3a.access.key", AWS_ACCESS_KEY) \
            .config("spark.hadoop.fs.s3a.secret.key", AWS_SECRET_KEY)
    spark = configure_spark_with_delta_pip(builder).getOrCreate()
    return spark


def main():
    load_dotenv()
    local = 1
    spark = config_spark()

    S3_BUCKET = os.getenv("S3_BUCKET")
    S3_FOLDER = os.getenv("DIOPSIS_S3")
    s3_path_raw = f"s3a://{S3_BUCKET}/{S3_FOLDER}/raw"
    local_raw = os.getenv("METADATA_LOCAL") + "raw/"

    path = local_raw if local else s3_path_raw
    LOCATION_ENTITY = 'location'
    RAW_LOCATION_TABLE = 'RAW_LOCATION'
    SENSOR_ENTITY = 'sensor'
    RAW_SENSOR_TABLE = 'RAW_SENSOR'
    DEPLOYMENT_ENTITY = 'deployment'
    RAW_DEPLOYMENT_TABLE = 'RAW_DEPLOYMENT'
    MEDIA_ITEM_ENTITY = "media_item"
    RAW_MEDIA_ITEM_TABLE = 'RAW_MEDIA_ITEM'


    # Create location raw table
    query = f'SELECT id, lat, lon, name FROM location_raw ORDER BY id;'
    df = spark.read.json(path + "locations.json")
    df.printSchema()
    df.write.format("delta").mode("overwrite").save(os.path.join(path, RAW_LOCATION_TABLE))
    df.createOrReplaceTempView("location_raw")
    df = spark.sql(f'SELECT id, lat, lon, name FROM location_raw ORDER BY id;')
    df.show(n=1000, truncate=False)

    # Create sensor raw table
    df = spark.read.json(path + "devices.json")
    df.printSchema()
    df.write.format("delta").mode("overwrite").save(os.path.join(path, RAW_SENSOR_TABLE))
    df.createOrReplaceTempView("sensor_raw")
    df = spark.sql(f'SELECT id, name, location_research_name FROM sensor_raw ORDER BY id;')
    df.show(n=1000, truncate=False)

    # Create deployment raw table
    df = spark.read.json(path + "deployments.json")
    df.printSchema()
    df.write.format("delta").mode("append").save(os.path.join(path, RAW_DEPLOYMENT_TABLE))
    df.createOrReplaceTempView("deployment_raw")
    df = spark.sql('SELECT id, device_name, device_id, location_id, start, end FROM deployment_raw;')
    df.show(n=1000, truncate=False)

    # Create raw media_item table from irods metadata
    img_md = get_image_metadata()
    df = spark.createDataFrame(img_md)
    df.show(truncate=False)
    df.write.format("delta").mode("overwrite").option("overwriteSchema", True).save(os.path.join(path, RAW_MEDIA_ITEM_TABLE))

    # Read again and query to be sure it was correctly written
    df = spark.read.format('delta').load(os.path.join(path, RAW_MEDIA_ITEM_TABLE))
    df.createOrReplaceTempView("media_item_raw")
    df = spark.sql('SELECT id, uri FROM media_item_raw;')
    df.show(n=1000, truncate=False)


if __name__ == "__main__":
    main()
