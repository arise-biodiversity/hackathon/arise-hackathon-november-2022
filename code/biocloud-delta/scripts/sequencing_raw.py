#!/usr/local/bin/python
import os
import sys
import pyspark
from delta import configure_spark_with_delta_pip
from dotenv import load_dotenv
import json
import glob

def config_spark(spark_path):
    SPARK_MASTER = "local[1]"

    builder = pyspark.sql.SparkSession.builder.appName("Biocloud").master(SPARK_MASTER) \
            .config("spark.sql.warehouse.dir", os.path.join(spark_path, "warehouse")) \
            .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
            .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
    spark = configure_spark_with_delta_pip(builder).getOrCreate()
    return spark

def convert_to_delta(spark, fname, raw_path, raw_name, multiline="false"):
    df = spark.read.option('multiline', multiline).json(fname)
    df.printSchema()
    df.write.format("delta").mode("overwrite").save(raw_path)
    df.createOrReplaceTempView(raw_name)

def main():
    load_dotenv()
    spark = config_spark()

    import_path = "/import/"
    export_path = "/export/"
    spark_path = "/spark"

    # Create raw delta tables for all json files in the import directory
    for fname in glob.glob(os.path.join(import_path, '*.json')):
        basename = os.path.basename(fname)
        raw_name = os.path.splitext(basename)[0] + '_raw'
        raw_path = os.path.join(export_path, raw_name)
        print('Converting', fname, 'to', raw_name, basename)

        try:
            convert_to_delta(spark, fname, raw_path, raw_name, multiline="false")
        except pyspark.sql.utils.AnalysisException as e:
            print('pyspark could not read JSON - trying with multiline option')
            convert_to_delta(spark, fname, raw_path, raw_name, multiline="true")


if __name__ == "__main__":
    main()
