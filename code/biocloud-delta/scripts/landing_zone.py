#!/usr/local/bin/python
import os
import time
import shutil
import sys
import json
from datetime import datetime
from os.path import isfile, join
from dotenv import load_dotenv
from connect_providers import Faunabit, Diopsis, S3, Irods
from pprint import pprint

def get_faunabit_metadata(local_folder):
  FAUNABIT_API_TYPES = ['locations', 'researches', 'devices']
  faunabit = Faunabit(os.getenv("FAUNABIT_URL"), os.getenv("FAUNABIT_TOKEN"))
  devices = []
  try:
    if not os.path.exists(local_folder):
          os.makedirs(local_folder)
    for type in FAUNABIT_API_TYPES:
        filename = os.path.join(local_folder, type + '.json')
        json_obj = faunabit.get_faunabit_data(type)
        if type == 'researches':
          # Get all deployments from researches
          deployments = []
          for obj in json_obj:
              for deployment in obj["location_researches"]:
                  deployments.append(deployment)
          write_local_obj(local_folder + "deployments.json", deployments)
        elif type == 'locations':
          # Get locations from locations
          write_local_obj(filename, json_obj["locations"])
        elif type == 'devices':
          write_local_obj(filename, json_obj)
          for obj in json_obj:
              devices.append({"number":obj["name"][8:],"id":obj["id"]})
  except OSError as error :
    print(error)
  return devices


def write_local_obj(filename, obj):
  """ Write json file to local folder """
  try:
      with open(filename, 'w') as outfile:
        outfile.write(json.dumps(obj))
      if not os.path.exists(filename):
        raise Exception('Metadata JSON file could not be saved in the storage')
  except Exception as e:
      print(f'An unexpected error occurred: {str(e)}' )


def delete_local_files(dir_path):
  """ Delete temp local folder"""
  try:
      if os.path.exists(dir_path):
        shutil.rmtree(dir_path)
        # temp = os.listdir( dir_path )
        # for item in temp:
        #     if item.endswith(".json"):
        #         os.remove( os.path.join( dir_path, item ) )
  except OSError as e:
      print("Error: %s : %s" % (dir_path, e.strerror))


def fetch_payload(client, path, camera_number, image_ids, total):
  """ Save images to local files """
  if not os.path.exists(path + camera_number):
      os.makedirs(path + camera_number)
  for image_id in image_ids[:total]:
      image_data = client.image_data(camera_number, image_id)
      image_file = f"{path}{camera_number}/{image_id}.jpg"
      with open(image_file, "wb") as f:
          f.write(image_data)
  print(f"{total} of {len(image_ids)} payload files stored in folder: {path}{camera_number}")


def get_image_data(camera_numbers, payload_path, images_to_fetch = 1):
  """ Fetch image payload from source """
  ENV_URL = os.getenv('DIOPSIS_URL')
  ENV_PASS = os.getenv('DIOPSIS_PASS')
  diopsis_client = Diopsis(ENV_URL, ENV_PASS)
  diopsis_client.login()

  # Get all ids from all cameras
  if camera_numbers:
    for camera_number in camera_numbers:
        image_ids = diopsis_client.image_ids_from_camera(camera_number)
        if image_ids:
          # Fetch images from server and save payload to local folder
          fetch_payload(diopsis_client, payload_path, camera_number, image_ids, images_to_fetch)
    return True
  else:
    print("No cameras selected. No images fetched")
    return False


def cleanup_irods(irods, coll):
  irods.ls(coll)
  irods.remove_collection(coll)
  print (f"Collection {coll} removed from IRODS")


def upload_irods_collection(irods, local_folder, collection):
  """ Upload local folder to irods """
  try:
    files = get_upload_files(local_folder)
    for file in files:
      filename = local_folder + "/" + file
      with irods.session as session:
        if ( session.data_objects.exists(collection+ "/" + file)):
          print (f"Object {collection}/{file} already exists")
        else:
          date = datetime.strptime(file[:14], '%Y%m%d%H%M%S')
          session.data_objects.put(filename, collection)
          #insert metadata
          obj = session.data_objects.get(collection + "/" + file)
          obj.metadata.add("datetime", str(date.date()) + ' ' + str(date.time()))
          obj.metadata.add("mime-type", "image/jpeg")
          pprint(obj.metadata.items())
    return True
  except Exception as e:
    print('An unexpected error occurred: ' + str(e) )
    return False


def upload_payload(local_folder, devices, clean):
  irods_home_path = os.getenv("ISTORAGE_ROOT")
  image_irods_folder = os.getenv("IMAGE_IRODS")
  collection = "/".join(["", os.getenv("IZONE"), "home", os.getenv("IUSER")]) + image_irods_folder

  irods_credentials = {}
  irods_credentials["host"] = os.getenv("IHOST")
  irods_credentials["port"] = os.getenv("IPORT")
  irods_credentials["user"] = os.getenv("IUSER")
  irods_credentials["password"] = os.getenv("IPASS")
  irods_credentials["zone"] = os.getenv("IZONE")
  irods_credentials["default_resource"] = os.getenv("IDEFAULT_RESOURCE")
  irods = Irods(irods_credentials)
  # cleanup_irods(irods, collection)

  print(f'* Start Upload folder {local_folder} to irods collection {collection}')
  start = time.perf_counter()
  for device in devices:
    folder = local_folder + device["number"]
    coll = collection + '/' + device["number"]
    with irods.session as session:
      try:
        if not session.collections.exists(coll):
          irods.create_collection(coll)
          # Add metadata to collection
          coll_obj = session.collections.get(coll)
          coll_obj.metadata.add("sensor_id", str(device["id"]))
          print("deviceID",device['id'], coll_obj.metadata.get_one("sensor_id"))
        if session.collections.exists(coll):
          upload_irods_collection(irods, folder, coll)
      except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print('An unexpected error occurred: ' + str(e) , " -----> ", exc_tb.tb_lineno )

  elapsed = (time.perf_counter() - start) * 1000 # ms
  print(f'Payload succesfully stored in landing zone. Elapsed time: {elapsed:.4f} ms.\n')
  if clean:
    cleanup_irods(irods, collection)
  return True


def s3_upload_metadata(source_folder):
  """ Store metadata in S3 bucket """
  store = 1
  remove = 1

  s3_folder = os.getenv("DIOPSIS_S3")
  s3_credentials = {}
  s3_credentials["s3_key_id"] = os.getenv("S3_KEY_ID")
  s3_credentials["s3_secret"] = os.getenv("S3_SECRET")
  s3_credentials["s3_bucket"] = os.getenv("S3_BUCKET")
  s3_credentials["s3_region"] = os.getenv("S3_REGION")
  s3 = S3(s3_credentials)
  s3.connect()
  s3.list_objects()

  if remove:
    s3.empty_bucket()
    # s3.remove_objects(s3_folder, files)
    s3.list_objects()

  files = get_upload_files(source_folder)

  if store:
    s3.store_objects(source_folder, s3_folder, files)
    s3.list_objects()
  return True


def get_upload_files(source_folder):
  """ Get all file names from local folder """
  files = []
  try:
    files = [file for file in os.listdir(source_folder) if isfile(join(source_folder, file))]
  except IOError:
      exit("Files to be uploaded are not accessible.")
  return files


def main():
  load_dotenv()

  # temp local files
  payload_folder = os.getenv("PAYLOAD_LOCAL")
  metadata_folder = os.getenv("METADATA_LOCAL") + "raw/"

  # Number of images and sensors to fetch
  tot_images = 7
  tot_sensors = 5

  clean_local = False
  clean_irods = False

  if clean_local:
    delete_local_files(metadata_folder)
    delete_local_files(payload_folder)

  print("\n===== GET DEVICE, LOCATION AND DEPLOYMENT METADATA =====\n")
  devices = get_faunabit_metadata(metadata_folder)

  print("\n===== UPLOAD METADATA =====\n")
  s3_upload_metadata(metadata_folder)

  print("\n===== GET IMAGE PAYLOAD =====\n")
  camera_numbers = [num["number"] for num in devices]
  print("CAMERA NUMBERS: ", camera_numbers[0:tot_sensors])
  payload = get_image_data(camera_numbers[0:tot_sensors], payload_folder, tot_images)
  if payload:

  # if True:
    print("\n===== UPLOAD PAYLOAD =====\n")
    upload_payload(payload_folder, devices[0:tot_sensors], clean_irods)
  else:
    # TODO: rollback metadata?
    pass


if __name__ == "__main__":
    main()
