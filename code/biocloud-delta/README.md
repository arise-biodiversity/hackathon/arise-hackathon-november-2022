# Biocloud Delta prototype

## Create [python enviroment](https://docs.python.org/3/tutorial/venv.html)

Create a virtual environment (it will create a folder *.venv* inside root folder) and activate it.  From the root of the project run:
```
$ python3 -m venv .venv
$ source .venv/bin/activate
```

## Install [spark](https://spark.apache.org/) and [delta](https://docs.delta.io/2.0.0/index.html) inside the python environment
* Make sure you have a file requirements.txt with the next lines inside
```text
pyspark==3.2.0
delta-spark==2.0.0
requests
python-dotenv
PyExifTool
python-irodsclient
boto3
```

* Install the requirements file
```
$ python3 -m pip install -r requirements.txt
```

## Run scripts

1) Create .env file and set the correct values of the keys.
```
$ cp .env.example .env
```
2) Make sure environment is activated, otherwise activate it:
```
$ source .venv/bin/activate
```
---
### 1. To run the *landing_zone.py* (to import and store diopsis data) script:
With activated environment run the python script
```
$ python3 scripts/landing_zone.py
```
---
### 2. To run the *diopsis_raw.py* (to transform json diopsis data to delta RAW tables) script:
> To run diopsis_raw.py it is required to have the json source files (deployments.json,devices.json, locations.json) in the `METADATA_LOCAL` folder indicated in the .env file and obtained from running the `landing_zone.py` script.

* To connect to S3, hadoop-aws has to be installed:
> ### STEPS to install hadoop-aws
> 1. First we need to find the version we need.
>   Search for "hadoop-*" inside `.venv/lib/<python_version>/site-packages/pyspark/jars`
>```
>$ ls -l hadoop-*
>```
>2. Go to https://mvnrepository.com/ and search for:
>```org.apache.hadoop » hadoop-aws```
>3. Download the needed jar version (from step 1).
>4. Download also the jar bundle that belongs to it:
>```com.amazonaws » aws-java-sdk-bundle```
>5. Save them inside ```.venv/lib/python3.8/site-packages/pyspark/jars```

With activated environment run the python script
```
$ python3 scripts/diopsis_raw.py
```
---


## Use the docker image

```
docker-compose run delta scripts/<name of the script>
```

This will build the docker image automatically.

If you want to rebuild the docker image you first need to remove the containers
and delete the docker image.


```
docker container prune
docker image rm biocloud/deltatest
```

## JSON to RAW to Enriched of the fake data

Run the following commands inside the folder `code/biocloud-delta`.

```shell
mkdir -p data/import
cp ../../resources/*.json data/import
cp ../../resources/NSR_nodes.tsv data/import
docker-compose run delta scripts/sequencing_raw.py
docker-compose run delta scripts/raw_to_enriched.py
```

OBS: You might need to use `chown -R` to fix permissions.

After that you will have a `data/export` folder with the RAW and Enriched tables, and a `/spark` folder with the enriched tables and the warehouse.
