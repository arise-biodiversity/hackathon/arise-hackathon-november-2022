# ARISE Hackathon November 2022

ARISE and the eScience Center are organising a hackathon to get a group of enthusiastic developers together to build a prototype for our DNA sequence database. DNA sequencing is a cost-effective method to identify species both from individual specimens as from environmental samples. One of the most pressing needs for the biodiversity research community at the moment is an authoritative reference database with DNA barcodes to map unidentified genetic materials to known species. This prototype would be a major step towards this reference database.

The prototype will build the pipelines to bring relevant metadata together in a data lakehouse that can be queried through an API, which will then generate output files with reference barcode data that can be used for analysis. We have already built a similar prototype for image data and are confident that we can use this knowledge to apply it to sequence data.

Read [CONTRIBUTING.md](Contributing.md) to see how you can contribute to this project
and which rules you have to follow to safely add and merge stuff into the repository.
